﻿
namespace EventLog_Perf
{
    using System;
    using System.Diagnostics;
    using System.Threading;
    using System.Threading.Tasks;

    class Program
    {
        private const string eventLogName = "Event Log Perf Test";
        private const int itemsToQueue = 4;  
        private const long maxLogSize = 1028032; // must be KB, must be multiple of 64

        private static long writeCounter = 0;
        private static Task[] tasks;
        private static bool cancelled = false;
        private static EventLog eventLog;

        static void Main(string[] args)
        {
            if (EventLog.SourceExists(eventLogName))
            {
                EventLog.Delete(eventLogName);
            }

            EventLog.CreateEventSource(eventLogName, eventLogName);
            eventLog = new EventLog(eventLogName, ".", eventLogName);
            eventLog.MaximumKilobytes = maxLogSize;

            tasks = new Task[itemsToQueue];

            var sw = Stopwatch.StartNew();
            
            for (int counter = 0; counter < itemsToQueue; counter++)
            {
                tasks[counter] = Task.Run(() => GoGoEventLogger());
            }

            Console.WriteLine("Press enter to stop");
            Console.ReadLine();
            cancelled = true;
            Task.WhenAll(tasks).Wait();
            sw.Stop();

            Console.WriteLine("DONE!");
            Console.WriteLine("We asked to log {0} events", writeCounter);
            Console.WriteLine("Speed is {0:0.00} / second", writeCounter / sw.Elapsed.TotalSeconds);
            Console.WriteLine("press enter to quit");
            Console.ReadLine();
        }

        private static void GoGoEventLogger()
        {
            while (!cancelled)
            {
                eventLog.WriteEntry(DateTime.UtcNow.ToLongTimeString());
                Interlocked.Increment(ref writeCounter);
            }
        }
    }
}
